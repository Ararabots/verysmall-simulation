from argparse import ArgumentParser
from typing import Tuple
import vision_module.sim.packet_pb2 as packet_pb2
import socket

# message = packet_pb2.Packet()
# cmd = message.cmd.robot_commands.add()

class FiraSimSender:

    UDP_IP = ""
    UDP_PORT = 20011

    def __init__(self) -> None:


        self.socket = socket.socket(socket.AF_INET,     # Internet
                                    socket.SOCK_DGRAM)  # UDP
    
        self.socket.connect((FiraSimSender.UDP_IP, FiraSimSender.UDP_PORT))


        self._packet_interface = packet_pb2.Packet()
    
    def _create_command(self, team: int,
                          id: int, 
                          left_speed: float,
                          right_speed: float) -> str:
        
        cmd = self._packet_interface.cmd.robot_commands.add()

        cmd.yellowteam = team
        cmd.id = id
        cmd.wheel_left = left_speed
        cmd.wheel_right = right_speed

        return self._packet_interface.SerializeToString()



    def send_packet(self, team: int,
                          id: int, 
                          left_speed: float,
                          right_speed: float) -> None:

        cmd_str = self._create_command(team, id, left_speed, right_speed)

        self.socket.sendall(cmd_str)


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--l", required=True, type=float)
    parser.add_argument("--r", required=True, type=float)

    args = parser.parse_args()

    sender = FiraSimSender()
    
    sender.send_packet(
        team=1,
        id=0,
        left_speed=args.l,
        right_speed=args.r
    )