xhost +
docker run -it --rm --net=host --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="/home/$USER/repos/:/home/repos/" \
    --workdir="/home/repos/verysmall-simulation" \
    --privileged ararabotsvsss/vsss-container:simulation_latest